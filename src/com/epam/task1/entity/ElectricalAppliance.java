package com.epam.task1.entity;

public class ElectricalAppliance{

	private Double cost;
	private Double power;
	private boolean isTurn;

	public ElectricalAppliance(Double cost, Double power) {
		super();
		this.cost = cost;
		this.power = power;
		this.isTurn = false;
	}

	public ElectricalAppliance() {

	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getPower() {
		return power;
	}

	public void setPower(Double power) {
		this.power = power;
	}

	public boolean isTurn() {
		return isTurn;
	}

	public void setTurn(boolean isTurn) {
		this.isTurn = isTurn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cost == null) ? 0 : cost.hashCode());
		result = prime * result + (isTurn ? 1231 : 1237);
		result = prime * result + ((power == null) ? 0 : power.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		ElectricalAppliance other = (ElectricalAppliance) obj;
		if (cost == null) {
			if (other.cost != null){
				return false;
			}
		} else if (!cost.equals(other.cost)){
			return false;
		}
		if (isTurn != other.isTurn){
			return false;
		}
		if (power == null) {
			if (other.power != null){
				return false;
			}
		} else if (!power.equals(other.power)){
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ElectricalAppliance [power=" + power+ ", cost=" + cost 
				+ ", isTurn=" + isTurn + ", getClass()=" + getClass() + "]";
	}
}