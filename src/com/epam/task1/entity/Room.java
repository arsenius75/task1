package com.epam.task1.entity;

import java.util.ArrayList;

public class Room {

	private ArrayList<ElectricalAppliance> applianceList = new ArrayList<ElectricalAppliance>();

	public ArrayList<ElectricalAppliance> getApplianceList() {
		return applianceList;
	}

	public void setApplianceList(ArrayList<ElectricalAppliance> applianceList) {
		this.applianceList = applianceList;
	}
	
	public void add(ElectricalAppliance i){
		applianceList.add(i);
	}
}
