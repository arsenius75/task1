package com.epam.task1.entity.impl;

import com.epam.task1.entity.ElectricalAppliance;

public class Iron extends ElectricalAppliance {

	public Iron(Double cost, Double power) {
		super(cost, power);
	}

	public Iron(){
		super();
	}
	
	private int maxTemperature;
	
	public int getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(int maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	@Override
	public String toString() {
		return "Iron [maxTemperature=" + maxTemperature + ", power=" + super.getPower()+ ", cost=" + super.getCost() 
				+ ", isTurn=" + super.isTurn() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + maxTemperature;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (!super.equals(obj)){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		Iron other = (Iron) obj;
		if (maxTemperature != other.maxTemperature){
			return false;
		}
		return true;
	}

	
	

}
