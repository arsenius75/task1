package com.epam.task1.entity.impl;

import com.epam.task1.entity.ElectricalAppliance;

public class Notebook extends ElectricalAppliance {
	
	public Notebook(Double cost, Double power) {
		super(cost, power);
	}

	public Notebook(){
		super();
	}
	
	private double weight;

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "Notebook [weight=" + weight + ", power=" + super.getPower()+ ", cost=" + super.getCost() 
				+ ", isTurn=" + super.isTurn() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(weight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (!super.equals(obj)){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		Notebook other = (Notebook) obj;
		if (Double.doubleToLongBits(weight) != Double.doubleToLongBits(other.weight)){
			return false;
		}
		return true;
	}

	
}
