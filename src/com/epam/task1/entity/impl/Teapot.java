package com.epam.task1.entity.impl;

import com.epam.task1.entity.ElectricalAppliance;

public class Teapot extends ElectricalAppliance {

	public Teapot(Double cost, Double power) {
		super(cost, power);
	}

	public Teapot(){
		super();
	}
	
	private int timeOfSimmer;
	
	public int getTimeOfSimmer() {
		return timeOfSimmer;
	}

	public void setTimeOfSimmer(int timeOfSimmer) {
		this.timeOfSimmer = timeOfSimmer;
	}

	@Override
	public String toString() {
		return "Teapot [timeOfSimmer=" + timeOfSimmer + ", power=" + super.getPower()+ ", cost=" + super.getCost() 
				+ ", isTurn=" + super.isTurn() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + timeOfSimmer;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (!super.equals(obj)){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		Teapot other = (Teapot) obj;
		if (timeOfSimmer != other.timeOfSimmer){
			return false;
		}
		return true;
	}
	
	
}
