package com.epam.task1.logic;

import com.epam.task1.entity.ElectricalAppliance;
import java.util.Comparator;

public class ApplianceComparator implements Comparator<Object> {

	@Override
	public int compare(Object f1, Object f2) {
		if (f1 == null){
			return 1;
		}
		if (f2 == null){
			return -1;
		}
		return ((ElectricalAppliance)f1).getPower().compareTo(((ElectricalAppliance)f2).getPower());
	}
}

