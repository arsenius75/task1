package com.epam.task1.logic;

import com.epam.task1.entity.ElectricalAppliance;
import com.epam.task1.entity.Room;

import java.util.ArrayList;
import java.util.Arrays;

public class ApplianceLogic {

	private Room room;
	private static ApplianceComparator applianceComparator= new ApplianceComparator();

	public ApplianceLogic(){
		room = new Room();
	}

	/**
	 * Add new object in ArrayList
	 * 
	 * @param name
	 * name of Class of this Object
	 * @param power
	 * @param cost
	 */
	public void addAppliance(String name, Double power, Double cost){

		ElectricalAppliance temp = ElectricalApplianceFactory.createAppliance(name);
		temp.setCost(cost);
		temp.setPower(power);
		temp.setTurn(false);
		room.add(temp);
	}

	/**
	 * find object, which parameters are between min and max
	 * 
	 * find and print every suitable object 
	 * 
	 * @param powerMin
	 * @param powerMax
	 * @param costMin
	 * @param costMax
	 */
	public Object[] findAppliance(Double powerMin, Double powerMax, Double costMin, Double costMax){

		ArrayList<ElectricalAppliance> foundList = new ArrayList<ElectricalAppliance>();


		for(ElectricalAppliance i : room.getApplianceList()){
			Double power = i.getPower();
			Double cost = i.getCost();

			if(powerMin <= power && powerMax >= power && costMin <= cost && costMax >= cost){
				foundList.add(i);				
			}
		}
		return foundList.toArray();
	}

	/**
	 * calls method setTurn(true) for objects with number n
	 * 
	 * turns on electricity appliance with number n
	 * @param n
	 */
	public void turnOnNumber(int n){
		room.getApplianceList().get(n).setTurn(true);
	}

	/**
	 * Counts a general power for all included electricity appliances
	 * 
	 * @return
	 */
	public Double countPower(){

		Double generalPower = .0;
		for(ElectricalAppliance i : room.getApplianceList()){
			if(i.isTurn()){
				generalPower += i.getPower();
			}

		}
		return generalPower;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	/**
	 * Sorts electricity appliances by power ascending
	 */
	public Object[] sort(){
		Object[] a = room.getApplianceList().toArray();
		Arrays.sort(a, applianceComparator);
		return a;

	}
}