package com.epam.task1.logic;

/**
 * enum with the names of Electrical appliances
 * @author �������
 *
 */
public enum ApplianceName{

	IRON,
	NOTEBOOK,
	TEAPOT
}
