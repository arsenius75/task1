package com.epam.task1.logic;

import com.epam.task1.entity.ElectricalAppliance;
import com.epam.task1.entity.impl.*;

/**
 * Electrical Appliance Factory!
 * @author �������
 *
 */
public final class ElectricalApplianceFactory {

	private ElectricalApplianceFactory(){}
	
	public final static ElectricalAppliance createAppliance(String name){
		
		
		ElectricalAppliance appliance;
		
		if(ApplianceName.IRON.toString().equalsIgnoreCase(name)){
			appliance = new Iron();
		}
		else if(ApplianceName.NOTEBOOK.toString().equalsIgnoreCase(name)){
			appliance = new Notebook();
		}
		else if(ApplianceName.TEAPOT.toString().equalsIgnoreCase(name)){
			appliance = new Teapot();
		}
		else{
			return null;
		}
		
		return appliance;
	}
}
