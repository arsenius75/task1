package com.epam.task1.main;

import com.epam.task1.entity.ElectricalAppliance;
import com.epam.task1.manager.ApplianceManager;
import com.epam.task1.manager.ApplianceManagerException;

public class Main {

	public static void main(String[] args) throws ApplianceManagerException {

		ApplianceManager manager = new ApplianceManager();

		manager.execute("add", "Iron", 3.3, 8.9);
		manager.execute("add", "Notebook", 34.3, 0.9);
		manager.execute("add", "Teapot", 7.3, 8.4);
		manager.execute("add", "Iron", 1.6, 6.1);
		manager.execute("add", "Notebook", 4.3, 5.9);
		manager.execute("add", "Teapot", 34.3, 8.0);
		manager.execute("add", "Iron", 3.34, 82.9);
		manager.execute("add", "Notebook", 23.3, 58.9);
		manager.execute("add", "Teapot", 32.4, 85.9);
		manager.execute("add", "Iron", 378.3, 844.9);

		
		Object[] unsortedArray = manager.execute("get list", "");
		System.out.println("Unsorted List");
		for(Object i : unsortedArray){
			System.out.println(i);
		}
		
		Object[] sortedArray = manager.execute("sort","");
		System.out.println("Sorted List");
		for(Object i : sortedArray){
			System.out.println(i);
		}

		manager.execute("turn", 0);
		manager.execute("turn", 2);
		manager.execute("turn", 3);
		manager.execute("turn", 6);
		manager.execute("turn", 8);
		
		System.out.println("General power is " + manager.execute("count power"));

		Object[] foundArray = manager.execute("find",2.0, 40.0, 58.0, 850.0);
		for(Object i : foundArray){
			System.out.println((ElectricalAppliance)i);
		}

	}

}
