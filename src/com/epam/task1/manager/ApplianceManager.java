package com.epam.task1.manager;

import com.epam.task1.logic.ApplianceLogic;

public class ApplianceManager {

	private ApplianceLogic logic = new ApplianceLogic();

	/**
	 * executes operations with logic
	 * 
	 * @param commandLine
	 * name of command
	 * 
	 * @param param
	 * parameters
	 * @return
	 * @throws ApplianceManagerException
	 */
	public Object[] execute (String commandLine , Object... param) throws ApplianceManagerException{

		switch (commandLine) {
		case "add":  {
			if(param[0].getClass() == String.class && param[1].getClass() == Double.class && param[2].getClass() == Double.class){
				logic.addAppliance((String)param[0], (Double)param[1], (Double)param[2]);
			}
		}
		break;

		case "find":{
			if(param[0].getClass() ==  Double.class && param[1].getClass() ==  Double.class && param[2].getClass() ==  Double.class && param[3].getClass() ==  Double.class){
				return	logic.findAppliance((Double)param[0], (Double)param[1], (Double)param[2], (Double)param[3]);
			}
		}
		break;

		case "turn": {
			if(param[0].getClass() ==  Integer.class){
				logic.turnOnNumber((int)param[0]);
			}

		}
		break;

		case "sort": {
			return logic.sort();
		}

		case "get list": {
			return logic.getRoom().getApplianceList().toArray();
		}

		default: throw new ApplianceManagerException();
		}
		return null;
	}

	public Double execute (String commandLine) throws ApplianceManagerException{

		if("count power".equals(commandLine)){
			return logic.countPower();
		}
		else
			throw new ApplianceManagerException();

	}
}
